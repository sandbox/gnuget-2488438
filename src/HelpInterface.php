<?php
/**
 * @file
 * Provides Drupal\advanced_help\HelpInterface
 */

namespace Drupal\advanced_help;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for ice cream flavor plugins.
 */
interface HelpInterface extends PluginInspectionInterface {
}